package ontheredbox.com.android.service

import com.google.gson.JsonObject
import io.reactivex.Observable
import ontheredbox.com.android.data.model.Account
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.Categories
import ontheredbox.com.android.data.model.CompletedCourses
import ontheredbox.com.android.data.model.Courses
import ontheredbox.com.android.data.model.Token
import ontheredbox.com.android.data.model.UserRegistration
import ontheredbox.com.android.utils.ApiConstants
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {

    @POST(ApiConstants.ENG_LANG + ApiConstants.LOGIN)
    fun tryToLogin(@Body params: JsonObject): Observable<Token>

    @POST(ApiConstants.ENG_LANG + ApiConstants.REGISTER)
    fun tryToRegister(@Body params: JsonObject): Observable<UserRegistration>

    @GET(ApiConstants.ENG_LANG + ApiConstants.ACCOUNT)
    fun getAccount(@Query("token") token: String): Observable<Account>

    @GET(ApiConstants.ENG_LANG + ApiConstants.ACCOUNT + ApiConstants.GET_USER)
    fun getUser(): Observable<Account.User>

    @GET(ApiConstants.ENG_LANG + ApiConstants.COURSES)
    fun getAllCourses(@Query("token") token: String): Observable<Courses>

    @GET(ApiConstants.ENG_LANG + ApiConstants.COURSES + ApiConstants.ACTIVE)
    fun getActiveCourses(@Query("token") token: String): Observable<ActiveCourses>

    @GET(ApiConstants.ENG_LANG + ApiConstants.COURSES + ApiConstants.COMPLETED)
    fun getCompletedCourses(@Query("token") token: String): Observable<CompletedCourses>

    @GET(ApiConstants.ENG_LANG + ApiConstants.CATEGORIES)
    fun getCategories(@Query("token") token: String): Observable<Categories>




}