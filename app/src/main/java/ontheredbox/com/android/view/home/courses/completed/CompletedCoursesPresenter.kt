package ontheredbox.com.android.view.home.courses.completed

import android.annotation.SuppressLint
import android.util.Log
import ontheredbox.com.android.repository.courses.CoursesRepositoryProvider

class CompletedCoursesPresenter(private val completedCoursesView: CompletedCoursesView) {
    @SuppressLint("CheckResult")
    fun getCompletedCourses(token: String) {
        CoursesRepositoryProvider.provideCoursesRepository()
            .getCompletedCourses(token)
            .doOnSubscribe { completedCoursesView.showLoading() }
            .doOnTerminate { completedCoursesView.hideLoading() }
            .doOnError { completedCoursesView.coursesFailed(it.localizedMessage) }
            .subscribe(
                { courses -> completedCoursesView.completedCoursesRetrieved(courses) },
                { error -> Log.d("AllCoursesPresenter", error.localizedMessage) })
    }
}