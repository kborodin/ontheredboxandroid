package ontheredbox.com.android.view.auth.registration

import ontheredbox.com.android.data.model.UserRegistration
import ontheredbox.com.android.view.general.LoadingView

interface RegistrationView: LoadingView {
    fun startApplicationScreen()
    fun showRegistrationError(error: String)
    fun getRegistrationResponse(userRegistration: UserRegistration)
}