package ontheredbox.com.android.view.auth.login

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login.toolbarLogin
import kotlinx.android.synthetic.main.fragment_reset_password.toolbarResetPass
import ontheredbox.com.android.R
import ontheredbox.com.android.utils.dpToPx

class ResetPasswordFragment: Fragment() {

    companion object {
        fun newInstance(): ResetPasswordFragment {
            return ResetPasswordFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val drawable = ContextCompat.getDrawable(activity!!, R.drawable.back_arrow)
        val bitmap = (drawable as BitmapDrawable).bitmap
        val newDrawable = BitmapDrawable(
            activity!!.resources, Bitmap.createScaledBitmap(
                bitmap, dpToPx(21),
                dpToPx(16), true
            )
        )
        toolbarResetPass.navigationIcon = newDrawable
        toolbarResetPass.setNavigationOnClickListener { activity!!.supportFragmentManager.popBackStack() }
    }
}