package ontheredbox.com.android.view.home.account

import android.annotation.SuppressLint
import ontheredbox.com.android.repository.account.AccountRepositoryProvider

class AccountPresenter(private val accountView: AccountView) {

    @SuppressLint("CheckResult")
    fun getAccount(token: String) {
        AccountRepositoryProvider.provideAccountRepository()
            .getAccount(token)
            .doOnSubscribe { accountView.showLoading() }
            .doOnTerminate { accountView.hideLoading() }
            .subscribe(
                { accountView.accountRetrieved() },
                { accountView.accountFailed() })
    }

    @SuppressLint("CheckResult")
    fun getUser(){
        AccountRepositoryProvider.provideAccountRepository()
            .getUser()
            .doOnSubscribe { accountView.showLoading() }
            .doOnTerminate { accountView.hideLoading() }
            .subscribe(
                { accountView.accountRetrieved() },
                { accountView.accountFailed() })
    }
}