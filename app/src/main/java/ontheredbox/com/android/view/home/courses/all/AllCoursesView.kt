package ontheredbox.com.android.view.home.courses.all

import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.Courses
import ontheredbox.com.android.data.model.Courses.CompletedCourses
import ontheredbox.com.android.view.general.LoadingView

interface AllCoursesView: LoadingView {
    fun allCoursesRetrieved(course: Courses)
    fun coursesFailed(error: String)
}