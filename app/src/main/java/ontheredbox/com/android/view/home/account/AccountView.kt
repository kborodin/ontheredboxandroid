package ontheredbox.com.android.view.home.account

import ontheredbox.com.android.view.general.LoadingView

interface AccountView: LoadingView {
    fun accountRetrieved()
    fun accountFailed()
}