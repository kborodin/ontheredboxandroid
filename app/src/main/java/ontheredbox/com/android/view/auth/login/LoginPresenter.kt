package ontheredbox.com.android.view.auth.login

import android.util.Log
import ontheredbox.com.android.repository.authorization.LoginRepositoryProvider
import ontheredbox.com.android.utils.PreferenceUtils

class LoginPresenter(private val loginView: LoginView) {

    fun init() {
        val token = PreferenceUtils.getToken()
        if (!token.isEmpty()) {
            Log.d("AuthPresenter", "Access Token = $token")
            loginView.startApplicationScreen()
        } else {
            Log.d("AuthPresenter", "Access Token is empty")
        }
    }

    fun tryToLogin(email: String, password: String) {
        when {
            email.isEmpty() -> loginView.showIncorrectEmail()
            password.isEmpty() -> loginView.showIncorrectPassword()
            else -> {
                LoginRepositoryProvider.provideLoginRepository()
                        .tryToLogin(email, password)
                        .doOnSubscribe { loginView.showLoading() }
                        .doOnTerminate { loginView.hideLoading() }
                        .subscribe(
                                { loginView.startApplicationScreen() },
                                { error -> loginView.loginError(error.localizedMessage) })
            }
        }
    }
}