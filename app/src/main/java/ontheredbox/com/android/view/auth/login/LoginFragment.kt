package ontheredbox.com.android.view.auth.login

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login.btnTryToLogin
import kotlinx.android.synthetic.main.fragment_login.etEmail
import kotlinx.android.synthetic.main.fragment_login.etPassword
import kotlinx.android.synthetic.main.fragment_login.llLoginContainer
import kotlinx.android.synthetic.main.fragment_login.progressBarLoginPage
import kotlinx.android.synthetic.main.fragment_login.toolbarLogin
import kotlinx.android.synthetic.main.fragment_login.tvForgotPassword
import ontheredbox.com.android.R
import ontheredbox.com.android.utils.PreferenceUtils
import ontheredbox.com.android.utils.dpToPx
import ontheredbox.com.android.utils.replaceFragment
import ontheredbox.com.android.utils.showToast
import ontheredbox.com.android.view.home.HomeActivity
import ontheredbox.com.android.view.home.account.AccountPresenter
import ontheredbox.com.android.view.home.account.AccountView

private val TAG = LoginFragment::class.java.simpleName

class LoginFragment : Fragment(), LoginView, AccountView {

    private lateinit var loginPresenter: LoginPresenter

    companion object {

        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity!! as AppCompatActivity).setSupportActionBar(toolbarLogin)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val drawable = ContextCompat.getDrawable(activity!!, R.drawable.back_arrow)
        val bitmap = (drawable as BitmapDrawable).bitmap
        val newDrawable = BitmapDrawable(
            activity!!.resources, Bitmap.createScaledBitmap(
                bitmap, dpToPx(21),
                dpToPx(16), true
            )
        )
        toolbarLogin.navigationIcon = newDrawable
        toolbarLogin.setNavigationOnClickListener { activity!!.supportFragmentManager.popBackStack() }
        loginPresenter = LoginPresenter(this)
//        loginPresenter.init()
        btnTryToLogin.setOnClickListener { tryToLogin() }
        tvForgotPassword.setOnClickListener {
            replaceFragment(
                activity = activity!!,
                container = R.id.rlContainer,
                fragment = ResetPasswordFragment.newInstance(),
                toBackStack = true,
                tag = ResetPasswordFragment::class.java.simpleName
            )
        }
    }
    //LoginView
    private fun tryToLogin() {
        loginPresenter.tryToLogin(etEmail.text.toString(), etPassword.text.toString())
    }

    override fun startApplicationScreen() {
        val intent = Intent(activity!!, HomeActivity::class.java)
        startActivity(intent)
        activity!!.finish()
    }

    override fun loginError(error: String) {
        llLoginContainer.visibility = View.VISIBLE
        showToast(activity!!, "You've entered an incorrect email or password", false)
    }

    override fun showIncorrectEmail() {
        showToast(activity!!, "Email is empty", false)
    }

    override fun showIncorrectPassword() {
        showToast(activity!!, "Password is empty", false)
    }
    //Account View
    override fun accountRetrieved() {
        Log.d(TAG, "Account retrieved")
    }

    override fun accountFailed() {
        Log.d(TAG, "Account failed")
    }

    override fun showLoading() {
        llLoginContainer.visibility = View.GONE
        progressBarLoginPage.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBarLoginPage.visibility = View.GONE
    }
}
