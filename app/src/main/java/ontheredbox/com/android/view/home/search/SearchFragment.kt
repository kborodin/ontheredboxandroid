package ontheredbox.com.android.view.home.search

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_search.layoutMainContent
import kotlinx.android.synthetic.main.fragment_search.recyclerSearchCourses
import kotlinx.android.synthetic.main.fragment_search.searchProgressBar
import kotlinx.android.synthetic.main.fragment_search.tvCoursesTitle
import ontheredbox.com.android.R
import ontheredbox.com.android.R.string
import ontheredbox.com.android.adapters.SearchCoursesAdapter
import ontheredbox.com.android.data.model.Courses
import ontheredbox.com.android.data.model.Courses.Course
import ontheredbox.com.android.utils.PreferenceUtils
import ontheredbox.com.android.utils.showToast
import ontheredbox.com.android.view.home.courses.all.AllCoursesPresenter
import ontheredbox.com.android.view.home.courses.all.AllCoursesView

class SearchFragment : Fragment(), AllCoursesView {
    private lateinit var searchCoursesAdapter: SearchCoursesAdapter
    private lateinit var coursesArray: ArrayList<Course>
    private lateinit var allCoursesPresenter: AllCoursesPresenter

    companion object {

        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerSearchCourses.layoutManager = GridLayoutManager(activity!!.applicationContext, 3)
        } else {
            recyclerSearchCourses.layoutManager = GridLayoutManager(activity!!.applicationContext, 6)
        }
        allCoursesPresenter = AllCoursesPresenter(this)
        allCoursesPresenter.getAllCourses(PreferenceUtils.getToken())
        coursesArray = ArrayList()
    }

    override fun allCoursesRetrieved(course: Courses) {
        coursesArray.addAll(course.courses)
        searchCoursesAdapter = SearchCoursesAdapter(activity!!, coursesArray)
        recyclerSearchCourses.adapter = searchCoursesAdapter
        tvCoursesTitle.text =
            String.format("${getString(string.courses)} (${coursesArray.size} ${getString(R.string.in_total)})")
    }

    override fun coursesFailed(error: String) {
        showToast(activity!!, "Can't find any course", false)
    }

    override fun showLoading() {
        searchProgressBar.visibility = View.VISIBLE
        layoutMainContent.visibility = View.GONE
    }

    override fun hideLoading() {
        searchProgressBar.visibility = View.GONE
        layoutMainContent.visibility = View.VISIBLE
    }
}