package ontheredbox.com.android.view.home.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import kotlinx.android.synthetic.main.fragment_wishlist.btnBack
import kotlinx.android.synthetic.main.fragment_wishlist.recyclerWishList
import ontheredbox.com.android.R
import ontheredbox.com.android.adapters.WishListAdapter
import ontheredbox.com.android.data.model.WishCourse

class WishListFragment : Fragment() {

    private lateinit var wishListAdapter: WishListAdapter
    private lateinit var wishList: ArrayList<WishCourse>

    companion object {
        fun newInstance(): WishListFragment {
            return WishListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_wishlist, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recyclerWishList.layoutManager = LinearLayoutManager(activity!!)
        btnBack.setOnClickListener { activity!!.supportFragmentManager.popBackStack() }
        wishList = ArrayList()
        getWishCourses()
        initAdapter()
    }

    private fun getWishCourses() {
        wishList.add(
            WishCourse(10, "OnTheRedBox: About Us", 280, 37)
        )
        wishList.add(
            WishCourse(8, "The Doctrines Of The Cross", 67, 5)
        )
        wishList.add(
            WishCourse(6, "Personal Evangelism", 114, 32)
        )
    }

    private fun initAdapter() {
        wishListAdapter = WishListAdapter(activity!!, wishList)
        recyclerWishList.adapter = wishListAdapter
    }
}