package ontheredbox.com.android.view.home.courses

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_courses.progressBarCourses
import kotlinx.android.synthetic.main.fragment_courses.recyclerAllCourses
import ontheredbox.com.android.R
import ontheredbox.com.android.adapters.ActiveCoursesAdapter
import ontheredbox.com.android.adapters.CompletedCoursesAdapter
import ontheredbox.com.android.adapters.AllCoursesAdapter
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.CompletedCourses
import ontheredbox.com.android.data.model.Courses
import ontheredbox.com.android.utils.COURSES_ACTIVE
import ontheredbox.com.android.utils.COURSES_ALL
import ontheredbox.com.android.utils.COURSES_BUNDLE_KEY
import ontheredbox.com.android.utils.COURSES_COMPLETED
import ontheredbox.com.android.utils.PreferenceUtils
import ontheredbox.com.android.view.home.courses.active.ActiveCoursesPresenter
import ontheredbox.com.android.view.home.courses.active.ActiveCoursesView
import ontheredbox.com.android.view.home.courses.all.AllCoursesPresenter
import ontheredbox.com.android.view.home.courses.all.AllCoursesView
import ontheredbox.com.android.view.home.courses.completed.CompletedCoursesPresenter
import ontheredbox.com.android.view.home.courses.completed.CompletedCoursesView

private val TAG = CoursesFragment::class.java.simpleName

class CoursesFragment : Fragment(), AllCoursesView,
    ActiveCoursesView, CompletedCoursesView {

    companion object {
        fun newInstance(page: String): CoursesFragment {
            val fragment = CoursesFragment()
            val bundle = Bundle()
            bundle.putString(COURSES_BUNDLE_KEY, page)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_courses, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (!arguments!!.isEmpty && arguments!!.containsKey(COURSES_BUNDLE_KEY)) {
            when (arguments!![COURSES_BUNDLE_KEY]) {
                COURSES_ALL -> {
                    val coursesPresenter = AllCoursesPresenter(
                        allCoursesView = this
                    )
                    coursesPresenter.getAllCourses(PreferenceUtils.getToken())
                }
                COURSES_ACTIVE -> {
                    val coursesPresenter =
                        ActiveCoursesPresenter(
                            activeCoursesView = this
                        )
                    coursesPresenter.getActiveCourses(PreferenceUtils.getToken())
                }
                COURSES_COMPLETED -> {
                    val coursesPresenter =
                        CompletedCoursesPresenter(
                            completedCoursesView = this
                        )
                    coursesPresenter.getCompletedCourses(PreferenceUtils.getToken())
                }
            }
        }

        if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerAllCourses.layoutManager = GridLayoutManager(activity!!.applicationContext, 2)
        } else {
            recyclerAllCourses.layoutManager = GridLayoutManager(activity!!.applicationContext, 4)
        }
    }

    override fun allCoursesRetrieved(course: Courses) {
        recyclerAllCourses.adapter = AllCoursesAdapter(activity!!.applicationContext, course.courses)
    }

    override fun activeCoursesRetrieved(activeCourses: ActiveCourses) {
        recyclerAllCourses.adapter =
            ActiveCoursesAdapter(activity!!.applicationContext, activeCourses.courses.data)
    }

    override fun completedCoursesRetrieved(completedCourses: CompletedCourses) {
        recyclerAllCourses.adapter =
            CompletedCoursesAdapter(activity!!.applicationContext, completedCourses.courses.data)
    }

    override fun coursesFailed(error: String) {
        Log.d(TAG, "courses Failed")
    }

    override fun showLoading() {
        progressBarCourses.visibility = View.VISIBLE
        recyclerAllCourses.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBarCourses.visibility = View.GONE
        recyclerAllCourses.visibility = View.VISIBLE
    }
}