package ontheredbox.com.android.view.home.dashboard

import ontheredbox.com.android.data.model.Categories
import ontheredbox.com.android.view.general.LoadingView

interface DashboardView: LoadingView {
    fun categoriesProvided(categories: Categories)
    fun categoriesFailed(error: String)
}