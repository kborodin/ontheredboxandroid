package ontheredbox.com.android.view.auth.registration

import android.annotation.SuppressLint
import ontheredbox.com.android.repository.authorization.LoginRepositoryProvider
import ontheredbox.com.android.repository.registration.RegistrationRepositoryProvider

class RegistrationPresenter(private val registrationView: RegistrationView) {

    @SuppressLint("CheckResult")
    fun tryToRegister(
        email: String, firstName: String, lastName: String, checkbox: Boolean,
        password: String, confirmPassword: String
    ) {
        RegistrationRepositoryProvider.provideRegistrationRepository()
            .tryToRegister(email, firstName, lastName, checkbox, password, confirmPassword)
            .subscribe(
                { result -> registrationView.getRegistrationResponse(result) },
                { error -> registrationView.showRegistrationError(error.localizedMessage) })
    }

    @SuppressLint("CheckResult")
    fun tryToLogin(email: String, password: String) {
        LoginRepositoryProvider.provideLoginRepository()
            .tryToLogin(email, password)
            .doOnSubscribe { registrationView.showLoading() }
            .doOnTerminate { registrationView.hideLoading() }
            .subscribe(
                { registrationView.startApplicationScreen() },
                { error -> registrationView.showRegistrationError(error.localizedMessage) })
    }
}