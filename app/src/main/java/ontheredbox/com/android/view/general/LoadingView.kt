package ontheredbox.com.android.view.general

interface LoadingView {
    fun showLoading()
    fun hideLoading()
}