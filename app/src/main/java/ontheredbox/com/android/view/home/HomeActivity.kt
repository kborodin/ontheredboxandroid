package ontheredbox.com.android.view.home

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_home.navigationView
import ontheredbox.com.android.R
import ontheredbox.com.android.R.string
import ontheredbox.com.android.utils.replaceFragment
import ontheredbox.com.android.view.home.chat.ChatFragment
import ontheredbox.com.android.view.home.courses.GeneralCoursesFragment
import ontheredbox.com.android.view.home.dashboard.DashboardFragment
import ontheredbox.com.android.view.home.search.SearchFragment
import ontheredbox.com.android.view.home.settings.SettingsFragment

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        Hawk.init(this).build()
        navigationView.itemIconTintList = null
        bottomNavigationListener()
        if (savedInstanceState == null) {
            navigationView.selectedItemId = R.id.itemDashboard
        }
    }

    private fun bottomNavigationListener() {
        navigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.itemDashboard -> replaceFragment(
                    activity = this,
                    container = R.id.container,
                    fragment = DashboardFragment.newInstance(),
                    toBackStack = false,
                    tag = DashboardFragment::class.java.simpleName
                )
                R.id.itemSearch -> {
                    replaceFragment(
                        activity = this,
                        container = R.id.container,
                        fragment = SearchFragment.newInstance(),
                        toBackStack = false,
                        tag = SearchFragment::class.java.simpleName
                    )
                }
                R.id.itemCourses -> {
                    replaceFragment(
                        activity = this,
                        container = R.id.container,
                        fragment = GeneralCoursesFragment.newInstance(),
                        toBackStack = false,
                        tag = SearchFragment::class.java.simpleName
                    )
                }
                R.id.itemChat -> {
                    replaceFragment(
                        activity = this,
                        container = R.id.container,
                        fragment = ChatFragment.newInstance(),
                        toBackStack = true,
                        tag = ChatFragment::class.java.simpleName
                    )
                }
                R.id.itemSettings -> {
                    replaceFragment(
                        activity = this,
                        container = R.id.container,
                        fragment = SettingsFragment.newInstance(),
                        toBackStack = false,
                        tag = SettingsFragment::class.java.simpleName
                    )
                }
            }
            true
        }
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setMessage(getString(string.are_you_sure_you_want_to_exit))
            .setCancelable(false)
            .setPositiveButton(getString(android.R.string.yes)) { _, _ -> this@HomeActivity.finish() }
            .setNegativeButton(getString(android.R.string.no), null)
            .show()
    }
}