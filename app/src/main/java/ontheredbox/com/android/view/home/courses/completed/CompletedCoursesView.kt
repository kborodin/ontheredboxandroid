package ontheredbox.com.android.view.home.courses.completed

import ontheredbox.com.android.data.model.CompletedCourses
import ontheredbox.com.android.view.general.LoadingView

interface CompletedCoursesView: LoadingView {
    fun completedCoursesRetrieved(completedCourses: CompletedCourses)
    fun coursesFailed(error: String)
}