package ontheredbox.com.android.view.auth.registration

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_registration.btnNext
import kotlinx.android.synthetic.main.fragment_registration.btnTryToRegister
import kotlinx.android.synthetic.main.fragment_registration.etConfirmPassword
import kotlinx.android.synthetic.main.fragment_registration.etEmail
import kotlinx.android.synthetic.main.fragment_registration.etFirstName
import kotlinx.android.synthetic.main.fragment_registration.etLastName
import kotlinx.android.synthetic.main.fragment_registration.etPassword
import kotlinx.android.synthetic.main.fragment_registration.llConfirmPassword
import kotlinx.android.synthetic.main.fragment_registration.llRegistration
import kotlinx.android.synthetic.main.fragment_registration.progressBarRegistrationPage
import kotlinx.android.synthetic.main.fragment_registration.toolbarRegistration
import kotlinx.android.synthetic.main.fragment_registration.tvTermsOfUse
import ontheredbox.com.android.R
import ontheredbox.com.android.data.model.UserRegistration
import ontheredbox.com.android.utils.dpToPx

private val TAG = RegistrationFragment::class.java.simpleName

class RegistrationFragment : Fragment(), RegistrationView {

    companion object {
        fun newInstance(): RegistrationFragment {
            return RegistrationFragment()
        }
    }

    private lateinit var registrationPresenter: RegistrationPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity!! as AppCompatActivity).setSupportActionBar(toolbarRegistration)

        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val drawable = ContextCompat.getDrawable(activity!!, R.drawable.back_arrow)
        val bitmap = (drawable as BitmapDrawable).bitmap
        val newDrawable = BitmapDrawable(
            activity!!.resources, Bitmap.createScaledBitmap(
                bitmap, dpToPx(21),
                dpToPx(16), true
            )
        )
        toolbarRegistration.navigationIcon = newDrawable
        toolbarRegistration.setNavigationOnClickListener {
            if(llRegistration.visibility == View.GONE) {
                llRegistration.visibility = View.VISIBLE
                llConfirmPassword.visibility = View.GONE
            } else {
                activity!!.supportFragmentManager.popBackStack()
            }
        }

        registrationPresenter = RegistrationPresenter(this)
        readTermsFromHtml()
        btnTryToRegister.setOnClickListener {
            registrationPresenter.tryToRegister(
                etEmail.text.toString(), etFirstName.text.toString(),
                etLastName.text.toString(), true, etPassword.text.toString(),
                etConfirmPassword.text.toString()
            )
        }
        btnNext.setOnClickListener {
            llRegistration.visibility = View.GONE
            llConfirmPassword.visibility = View.VISIBLE
        }
    }

    private fun readTermsFromHtml() {
        tvTermsOfUse.text = Html.fromHtml(getString(R.string.by_registering_you_agree_to_our_terms_conditions))
        tvTermsOfUse.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun getRegistrationResponse(userRegistration: UserRegistration) {
        if (userRegistration.token.isNotEmpty()) {
            registrationPresenter.tryToLogin(userRegistration.user.email, etPassword.text.toString())
        } else {
            Log.d(TAG, "user token is empty")
        }
    }

    override fun startApplicationScreen() {
        Log.d(TAG, "startApplicationScreen called")
    }

    override fun showRegistrationError(error: String) {
        Log.d(TAG, "Error occurred: $error")
    }

    override fun showLoading() {
        llRegistration.visibility = View.GONE
        progressBarRegistrationPage.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        llRegistration.visibility = View.VISIBLE
        progressBarRegistrationPage.visibility = View.GONE
    }
}
