package ontheredbox.com.android.view.home.courses.open_course

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class CourseTitleFragment : Fragment() {

    companion object {
        fun newInstance(): CourseTitleFragment {
            return CourseTitleFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}