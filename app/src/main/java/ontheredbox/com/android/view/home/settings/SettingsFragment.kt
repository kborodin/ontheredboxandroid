package ontheredbox.com.android.view.home.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_settings.layoutWishList
import ontheredbox.com.android.R
import ontheredbox.com.android.utils.replaceFragment

class SettingsFragment : Fragment() {

    companion object {
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        layoutWishList.setOnClickListener {
            replaceFragment(activity!!, R.id.container, WishListFragment.newInstance(), true, "WishListFragment")
        }
    }
}