package ontheredbox.com.android.view.home.courses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_general_courses.coursesTabLayout
import kotlinx.android.synthetic.main.fragment_general_courses.coursesViewPager
import ontheredbox.com.android.R
import ontheredbox.com.android.R.string
import ontheredbox.com.android.adapters.CoursesPagerAdapter
import ontheredbox.com.android.utils.COURSES_ACTIVE
import ontheredbox.com.android.utils.COURSES_ALL
import ontheredbox.com.android.utils.COURSES_COMPLETED

class GeneralCoursesFragment: Fragment() {
    private lateinit var coursesPagerAdapter: CoursesPagerAdapter
    private lateinit var fragmentsArray: ArrayList<Fragment>
    private lateinit var titlesArray: ArrayList<String>

    companion object {
        fun newInstance(): GeneralCoursesFragment {
            return GeneralCoursesFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_general_courses, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initFragments()
        initTitles()
        coursesPagerAdapter = CoursesPagerAdapter(activity!!.supportFragmentManager, fragmentsArray, titlesArray)
        coursesViewPager.adapter = coursesPagerAdapter
        coursesTabLayout.setupWithViewPager(coursesViewPager)
    }

    private fun initFragments(){
        fragmentsArray = ArrayList()
        fragmentsArray.add(CoursesFragment.newInstance(COURSES_ALL))
        fragmentsArray.add(CoursesFragment.newInstance(COURSES_ACTIVE))
        fragmentsArray.add(CoursesFragment.newInstance(COURSES_COMPLETED))
    }

    private fun initTitles() {
        titlesArray = ArrayList()
        titlesArray.add(getString(string.all))
        titlesArray.add(getString(string.active))
        titlesArray.add(getString(string.completed))
    }
}