package ontheredbox.com.android.view.home.courses.all

import android.annotation.SuppressLint
import android.util.Log
import ontheredbox.com.android.repository.courses.CoursesRepositoryProvider

class AllCoursesPresenter(
    private val allCoursesView: AllCoursesView
) {

    @SuppressLint("CheckResult")
    fun getAllCourses(token: String) {
        CoursesRepositoryProvider.provideCoursesRepository()
            .getAllCourses(token)
            .doOnSubscribe { allCoursesView.showLoading() }
            .doOnTerminate { allCoursesView.hideLoading() }
            .doOnError { allCoursesView.coursesFailed(it.localizedMessage) }
            .subscribe(
                { courses -> allCoursesView.allCoursesRetrieved(courses) },
                { error -> Log.d("AllCoursesPresenter", error.localizedMessage) })
    }
}