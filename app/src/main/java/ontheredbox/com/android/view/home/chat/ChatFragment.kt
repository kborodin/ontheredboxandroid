package ontheredbox.com.android.view.home.chat

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_chat.btnBack
import kotlinx.android.synthetic.main.fragment_chat.btnSendMessage
import kotlinx.android.synthetic.main.fragment_chat.checkBox
import kotlinx.android.synthetic.main.fragment_chat.etChat
import kotlinx.android.synthetic.main.fragment_chat.recyclerChat
import ontheredbox.com.android.R
import ontheredbox.com.android.adapters.ChatAdapter
import ontheredbox.com.android.data.model.Message
import ontheredbox.com.android.utils.VIEW_TYPE_MESSAGE_RECEIVED
import ontheredbox.com.android.utils.VIEW_TYPE_MESSAGE_SEND

class ChatFragment : Fragment() {

    private lateinit var messageList: ArrayList<Message>
    private lateinit var chatAdapter: ChatAdapter

    companion object {
        fun newInstance(): ChatFragment {
            return ChatFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val bottomView = activity!!.findViewById<BottomNavigationView>(R.id.navigationView)
        etChat.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                bottomView.visibility = View.GONE
            }
        }
        btnBack.setOnClickListener {
            etChat.clearFocus()
            btnBack.requestFocus()
            bottomView.visibility = View.VISIBLE
        }
        initAdapter()
        btnSendMessage.setOnClickListener { sendMessage() }
    }

    private fun initAdapter() {
        recyclerChat.layoutManager = LinearLayoutManager(activity!!)
        messageList = ArrayList()
        chatAdapter = ChatAdapter(messageList)
        recyclerChat.adapter = chatAdapter
    }

    private fun sendMessage() {
        if(etChat.text!!.isNotEmpty()) {
            val typeOfMessage = if(checkBox.isChecked) {
                VIEW_TYPE_MESSAGE_SEND
            } else {
                VIEW_TYPE_MESSAGE_RECEIVED
            }
            messageList.add(Message(typeOfMessage, etChat.text.toString()))
            chatAdapter.notifyDataSetChanged()
            recyclerChat.smoothScrollToPosition(messageList.size)
        }
    }
}