package ontheredbox.com.android.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ontheredbox.com.android.R.layout

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
    }
}
