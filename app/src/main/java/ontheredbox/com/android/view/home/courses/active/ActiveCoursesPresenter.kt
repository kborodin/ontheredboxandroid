package ontheredbox.com.android.view.home.courses.active

import android.annotation.SuppressLint
import android.util.Log
import ontheredbox.com.android.repository.courses.CoursesRepositoryProvider

class ActiveCoursesPresenter(private val activeCoursesView: ActiveCoursesView) {

    @SuppressLint("CheckResult")
    fun getActiveCourses(token: String) {
        CoursesRepositoryProvider.provideCoursesRepository()
            .getActiveCourses(token)
            .doOnSubscribe { activeCoursesView.showLoading() }
            .doOnTerminate { activeCoursesView.hideLoading() }
            .doOnError { activeCoursesView.coursesFailed(it.localizedMessage) }
            .subscribe(
                { courses -> activeCoursesView.activeCoursesRetrieved(courses) },
                { error -> Log.d("AllCoursesPresenter", error.localizedMessage) })
    }
}