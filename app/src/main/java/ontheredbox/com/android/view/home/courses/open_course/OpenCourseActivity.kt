package ontheredbox.com.android.view.home.courses.open_course

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.activity_open_course.verticalViewPager

class OpenCourseActivity: AppCompatActivity() {

    private lateinit var fragmentsArray: ArrayList<Fragment>
    private lateinit var pagerAdapter: ScreenSlidePagerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentsArray = ArrayList()

    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {

        override fun getCount(): Int {
            return fragmentsArray.size
        }

        override fun getItem(position: Int): Fragment {
            return fragmentsArray[position]
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }
    }

    override fun onBackPressed() {
        if (verticalViewPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            verticalViewPager.currentItem = verticalViewPager.currentItem - 1
        }
    }
}