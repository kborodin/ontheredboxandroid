package ontheredbox.com.android.view.home.dashboard

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_dashboard.dashboardProgressBar
import kotlinx.android.synthetic.main.fragment_dashboard.layoutMainContent
import kotlinx.android.synthetic.main.fragment_dashboard.recyclerCategories
import kotlinx.android.synthetic.main.fragment_dashboard.recyclerLatestCourses
import kotlinx.android.synthetic.main.fragment_dashboard.tvSeeAll
import ontheredbox.com.android.R
import ontheredbox.com.android.adapters.CategoriesAdapter
import ontheredbox.com.android.adapters.LatestCoursesAdapter
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.Categories
import ontheredbox.com.android.utils.COURSES_ALL
import ontheredbox.com.android.utils.PreferenceUtils
import ontheredbox.com.android.utils.replaceFragment
import ontheredbox.com.android.view.home.courses.active.ActiveCoursesPresenter
import ontheredbox.com.android.view.home.courses.active.ActiveCoursesView
import ontheredbox.com.android.view.home.courses.CoursesFragment

private val TAG = DashboardFragment::class.java.simpleName

class DashboardFragment : Fragment(), DashboardView,
    ActiveCoursesView {

    private lateinit var dashboardPresenter: DashboardPresenter
    private lateinit var coursesPresenter: ActiveCoursesPresenter
    private var isFirstRun = true

    companion object {
        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerCategories.layoutManager = GridLayoutManager(activity!!.applicationContext, 3)
        } else {
            recyclerCategories.layoutManager = GridLayoutManager(activity!!.applicationContext, 6)
        }
        dashboardPresenter = DashboardPresenter(this)
        coursesPresenter = ActiveCoursesPresenter(this)
        if (PreferenceUtils.getCategories().size != 1) { //default size of PreferenceUtils.getCategories() is 1 item
            isFirstRun = false
            coursesPresenter.getActiveCourses(PreferenceUtils.getToken())
        }

        dashboardPresenter.getGategories(PreferenceUtils.getToken())

        tvSeeAll.setOnClickListener {
            replaceFragment(
                activity = activity!!,
                container = R.id.container,
                fragment = CoursesFragment.newInstance(COURSES_ALL),
                toBackStack = true,
                tag = CoursesFragment::class.java.simpleName
            )
        }
    }

    override fun categoriesProvided(categories: Categories) {
        var previousCategory = ""
        val listOfCategories = ArrayList<Categories.Category>()

        categories.categories.forEach {
            if (previousCategory != it.title) {
                listOfCategories.add(it)
                previousCategory = it.title
            }
        }
        PreferenceUtils.clearCategories()
        PreferenceUtils.saveCategories(listOfCategories)
        if (isFirstRun) {
            coursesPresenter.getActiveCourses(PreferenceUtils.getToken())
        }
        recyclerCategories.adapter = CategoriesAdapter(
            context = activity!!.applicationContext,
            categories = listOfCategories
        )
    }

    override fun categoriesFailed(error: String) {
        Log.d(TAG, "categoriesFailed: $error")
    }

    override fun showLoading() {
        dashboardProgressBar.visibility = View.VISIBLE
        layoutMainContent.visibility = View.GONE
    }

    override fun hideLoading() {
        dashboardProgressBar.visibility = View.GONE
        layoutMainContent.visibility = View.VISIBLE
    }

    override fun activeCoursesRetrieved(activeCourses: ActiveCourses) {
        recyclerLatestCourses.layoutManager = LinearLayoutManager(
            activity!!, LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerLatestCourses.adapter = LatestCoursesAdapter(
            context = activity!!,
            courses = activeCourses.courses.data,
            categories = PreferenceUtils.getCategories()
        )
    }

    override fun coursesFailed(error: String) {
        Log.d(TAG, "coursesFailed: $error")
    }
}