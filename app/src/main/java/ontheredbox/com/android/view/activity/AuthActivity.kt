package ontheredbox.com.android.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.orhanobut.hawk.Hawk
import ontheredbox.com.android.MyApp
import ontheredbox.com.android.R
import ontheredbox.com.android.utils.replaceFragment
import ontheredbox.com.android.view.auth.login.WelcomeFragment

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        Hawk.init(this).build()
        replaceFragment(this, R.id.rlContainer, WelcomeFragment.newInstance(),
            false, "WelcomeFragment")
    }
}