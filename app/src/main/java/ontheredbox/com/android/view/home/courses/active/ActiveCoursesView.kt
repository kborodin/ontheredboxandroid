package ontheredbox.com.android.view.home.courses.active

import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.view.general.LoadingView

interface ActiveCoursesView : LoadingView{
    fun activeCoursesRetrieved(activeCourses: ActiveCourses)
    fun coursesFailed(error: String)
}