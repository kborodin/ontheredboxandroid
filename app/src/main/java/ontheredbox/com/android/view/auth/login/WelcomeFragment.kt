package ontheredbox.com.android.view.auth.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_welcome.btnGetStarted
import kotlinx.android.synthetic.main.fragment_welcome.btnLoginPage
import ontheredbox.com.android.R
import ontheredbox.com.android.utils.replaceFragment
import ontheredbox.com.android.view.auth.registration.RegistrationFragment

class WelcomeFragment : Fragment() {

    companion object {
        fun newInstance(): WelcomeFragment {
            return WelcomeFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var toBackStack = false
        if (activity!!.supportFragmentManager.backStackEntryCount < 2) {
            toBackStack = true
        }
        btnGetStarted.setOnClickListener {
            replaceFragment(
                activity!!, R.id.rlContainer,
                RegistrationFragment.newInstance(), toBackStack, "RegistrationFragment"
            )
        }
        btnLoginPage.setOnClickListener {
            replaceFragment(
                activity!!, R.id.rlContainer,
                LoginFragment.newInstance(), toBackStack, "RegistrationFragment"
            )
        }
    }
}