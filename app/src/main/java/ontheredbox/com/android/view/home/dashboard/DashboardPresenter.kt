package ontheredbox.com.android.view.home.dashboard

import android.annotation.SuppressLint
import android.util.Log
import ontheredbox.com.android.repository.categories.CategoriesRepositoryProvider

class DashboardPresenter(private val dashboardView: DashboardView) {

    @SuppressLint("CheckResult")
    fun getGategories(token: String) {
        CategoriesRepositoryProvider.provideCategoriesRepository()
            .getCategories(token)
            .doOnSubscribe { dashboardView.showLoading() }
            .doOnTerminate { dashboardView.hideLoading() }
            .doOnError { dashboardView.categoriesFailed(it.localizedMessage) }
            .subscribe(
                { categories -> dashboardView.categoriesProvided(categories) },
                { error -> Log.d("AllCoursesPresenter", error.localizedMessage) })
    }
}