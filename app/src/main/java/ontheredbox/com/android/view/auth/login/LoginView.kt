package ontheredbox.com.android.view.auth.login

import ontheredbox.com.android.view.general.LoadingView

interface LoginView : LoadingView {
    fun startApplicationScreen()
    fun loginError(error: String)
    fun showIncorrectEmail()
    fun showIncorrectPassword()
}