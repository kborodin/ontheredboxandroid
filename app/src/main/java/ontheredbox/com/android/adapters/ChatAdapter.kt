package ontheredbox.com.android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_message_received.view.tvSentMessage
import ontheredbox.com.android.R
import ontheredbox.com.android.adapters.ChatAdapter.BaseViewHolder
import ontheredbox.com.android.data.model.Message
import ontheredbox.com.android.utils.VIEW_TYPE_MESSAGE_RECEIVED
import ontheredbox.com.android.utils.VIEW_TYPE_MESSAGE_SEND

class ChatAdapter(private val messageList: ArrayList<Message>) :
    RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun getItemCount(): Int = messageList.size

    override fun getItemViewType(position: Int): Int {
        val message = messageList[position]
        return if (message.userId == VIEW_TYPE_MESSAGE_SEND) {
            VIEW_TYPE_MESSAGE_SEND
        } else {
            VIEW_TYPE_MESSAGE_RECEIVED
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            VIEW_TYPE_MESSAGE_SEND -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_sent, parent, false)
                SendMessageHolder(view)
            }
            VIEW_TYPE_MESSAGE_RECEIVED -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_received, parent, false)
                ReceivedMessageHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val message = messageList[position]
        when(holder) {
            is SendMessageHolder -> holder.bind(message)
            is ReceivedMessageHolder -> holder.bind(message)
        }
    }

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(type: T)
    }

    class ReceivedMessageHolder(itemView: View) : BaseViewHolder<Message>(itemView) {
        private val tvSentMessage = itemView.tvSentMessage
        override fun bind(type: Message) {
            tvSentMessage.text = type.message
        }
    }

    class SendMessageHolder(itemView: View) : BaseViewHolder<Message>(itemView) {
        private val tvSentMessage = itemView.tvSentMessage
        override fun bind(type: Message) {
            tvSentMessage.text = type.message
        }
    }
}