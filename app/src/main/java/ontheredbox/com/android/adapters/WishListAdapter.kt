package ontheredbox.com.android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_wishlist.view.ivCourse
import kotlinx.android.synthetic.main.item_wishlist.view.tvAmountOfLessons
import kotlinx.android.synthetic.main.item_wishlist.view.tvAmountOfStudents
import kotlinx.android.synthetic.main.item_wishlist.view.tvCourseName
import ontheredbox.com.android.R
import ontheredbox.com.android.data.model.WishCourse
import ontheredbox.com.android.utils.COURSE_ABOUT_US_ID
import ontheredbox.com.android.utils.COURSE_THE_DOCTRINES_OF_THE_CROSS
import ontheredbox.com.android.utils.FORMING_AN_EVANGELISTIC_TEAM
import ontheredbox.com.android.utils.HOT_TO_PREPARE_YOUR_TESTIMONY
import ontheredbox.com.android.utils.ILLUSTRATED_MESSAGES
import ontheredbox.com.android.utils.PERSONAL_EVANGELISM
import ontheredbox.com.android.utils.THE_FOUR_COLUMNS

class WishListAdapter(private val context: Context, private val wishList: ArrayList<WishCourse>) :
    RecyclerView.Adapter<WishListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_wishlist, parent,
                false
            )
        )

    override fun getItemCount(): Int = wishList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val course = wishList[position]
        holder.tvCourseName.text = course.title
        holder.tvAmountOfLessons.text = course.amountOfLessons.toString()
        holder.tvAmountOfStudents.text = course.amountOfStudents.toString()
        setImageToCourse(course, holder.ivCourse)
    }

    private fun setImageToCourse(course: WishCourse, imageView: ImageView) {
        when (course.id) {
            COURSE_ABOUT_US_ID -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_about_us
                )
            )

            COURSE_THE_DOCTRINES_OF_THE_CROSS -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.the_doctrine_of_the_cross
                )
            )
            FORMING_AN_EVANGELISTIC_TEAM -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.forming_an_evangelistic_team
                )
            )
            PERSONAL_EVANGELISM -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.circle_personal_evangelism
                )
            )
            ILLUSTRATED_MESSAGES -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.illustrated_messages
                )
            )
            THE_FOUR_COLUMNS -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.the_four_columns
                )
            )
            HOT_TO_PREPARE_YOUR_TESTIMONY -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.how_to_prepare_your_testimony
                )
            )
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivCourse = itemView.ivCourse!!
        val tvCourseName = itemView.tvCourseName!!
        val tvAmountOfStudents = itemView.tvAmountOfStudents!!
        val tvAmountOfLessons = itemView.tvAmountOfLessons!!
    }
}