package ontheredbox.com.android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_latest.view.ivCourse
import kotlinx.android.synthetic.main.item_latest.view.tvCategoryName
import kotlinx.android.synthetic.main.item_latest.view.tvCourseName
import ontheredbox.com.android.R
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.Categories
import ontheredbox.com.android.utils.COURSE_ABOUT_US_ID
import ontheredbox.com.android.utils.COURSE_THE_DOCTRINES_OF_THE_CROSS
import ontheredbox.com.android.utils.FORMING_AN_EVANGELISTIC_TEAM
import ontheredbox.com.android.utils.HOT_TO_PREPARE_YOUR_TESTIMONY
import ontheredbox.com.android.utils.ILLUSTRATED_MESSAGES
import ontheredbox.com.android.utils.PERSONAL_EVANGELISM
import ontheredbox.com.android.utils.THE_FOUR_COLUMNS

class LatestCoursesAdapter(
    private val context: Context, private val courses: List<ActiveCourses.Courses.Data>,
    private val categories: List<Categories.Category>
) :
    RecyclerView.Adapter<LatestCoursesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_latest, parent,
                false
            )
        )

    override fun getItemCount(): Int = courses.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val course = courses[position]
        holder.tvCourseName.text = course.title
        categories.forEach {
            if (it.id == course.categoryId) {
                holder.tvCategoryName.text = it.title
            }
        }
        setImageToCourse(course, holder.ivCourse)
    }

    private fun setImageToCourse(course: ActiveCourses.Courses.Data, imageView: ImageView) {
        when (course.id) {
            COURSE_ABOUT_US_ID -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_about_us
                )
            )

            COURSE_THE_DOCTRINES_OF_THE_CROSS -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_the_doctrine_of_the_cross
                )
            )
            FORMING_AN_EVANGELISTIC_TEAM -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_forming_an_evangelistic_team
                )
            )
            PERSONAL_EVANGELISM -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_personal_evangelism
                )
            )
            ILLUSTRATED_MESSAGES -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_illustrated_messages
                )
            )
            THE_FOUR_COLUMNS -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.the_four_columns
                )
            )
            HOT_TO_PREPARE_YOUR_TESTIMONY -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_how_to_prepare_your_testimony
                )
            )
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivCourse = itemView.ivCourse!!
        val tvCourseName = itemView.tvCourseName!!
        val tvCategoryName = itemView.tvCategoryName!!
    }
}