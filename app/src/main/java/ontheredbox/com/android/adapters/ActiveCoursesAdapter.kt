package ontheredbox.com.android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_courses.view.ivCourse
import kotlinx.android.synthetic.main.item_courses.view.ivOptions
import kotlinx.android.synthetic.main.item_courses.view.progressOfCourse
import kotlinx.android.synthetic.main.item_courses.view.tvCourseName
import ontheredbox.com.android.R
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.CompletedCourses
import ontheredbox.com.android.utils.COURSE_ABOUT_US_ID
import ontheredbox.com.android.utils.COURSE_THE_DOCTRINES_OF_THE_CROSS
import ontheredbox.com.android.utils.FORMING_AN_EVANGELISTIC_TEAM
import ontheredbox.com.android.utils.HOT_TO_PREPARE_YOUR_TESTIMONY
import ontheredbox.com.android.utils.ILLUSTRATED_MESSAGES
import ontheredbox.com.android.utils.PERSONAL_EVANGELISM
import ontheredbox.com.android.utils.THE_FOUR_COLUMNS

class ActiveCoursesAdapter (private val context: Context, private val coursesList: List<ActiveCourses.Courses.Data>) :
    RecyclerView.Adapter<ActiveCoursesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActiveCoursesAdapter.ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_courses, parent,
                false
            )
        )

    override fun getItemCount(): Int = coursesList.size

    override fun onBindViewHolder(holder: ActiveCoursesAdapter.ViewHolder, position: Int) {
        val item = coursesList[position]
        holder.tvCourseName.text = item.title
        holder.progressOfCourse.progress = 30
        setImageToCourse(item, holder.ivCourse)
    }
    private fun setImageToCourse(course: ActiveCourses.Courses.Data, imageView: ImageView) {
        when (course.id) {
            COURSE_ABOUT_US_ID -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.rect_about_us
                )
            )

            COURSE_THE_DOCTRINES_OF_THE_CROSS -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.the_doctrine_of_the_cross
                )
            )
            FORMING_AN_EVANGELISTIC_TEAM -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.forming_an_evangelistic_team
                )
            )
            PERSONAL_EVANGELISM -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.circle_personal_evangelism
                )
            )
            ILLUSTRATED_MESSAGES -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.illustrated_messages
                )
            )
            THE_FOUR_COLUMNS -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.the_four_columns
                )
            )
            HOT_TO_PREPARE_YOUR_TESTIMONY -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.how_to_prepare_your_testimony
                )
            )
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivCourse = itemView.ivCourse!!
        val ivOptions = itemView.ivOptions!!
        val tvCourseName = itemView.tvCourseName!!
        val progressOfCourse = itemView.progressOfCourse!!
    }

}