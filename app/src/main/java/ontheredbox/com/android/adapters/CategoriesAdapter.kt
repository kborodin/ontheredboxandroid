package ontheredbox.com.android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_categories.view.ivCategory
import kotlinx.android.synthetic.main.item_categories.view.tvCategory
import ontheredbox.com.android.R
import ontheredbox.com.android.R.string
import ontheredbox.com.android.data.model.Categories

class CategoriesAdapter(private val context: Context, private val categories: List<Categories.Category>) :
    RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_categories, parent,
                false
            )
        )

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = categories[position]
        holder.tvCategory.text = item.title
        when (item.title) {
            context.getString(string.life_of_evangelizer) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.evangelizer))
            }
            context.getString(string.personal_evangelizm) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.personal_evangelism))
            }
            context.getString(string.messages) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.illstrated_message))
            }
            context.getString(string.testimonies) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.testimony))
            }
            context.getString(string.teams) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.teams))
            }
            context.getString(string.music) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.music))
            }
            context.getString(string.doctrine) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.doctrine))
            }
            context.getString(string.basic_training) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.basic_training))
            }
            context.getString(string.network) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.teams))
            }
            context.getString(string.basic_training_bt) -> {
                holder.ivCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.basic_training))
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivCategory = itemView.ivCategory!!
        val tvCategory = itemView.tvCategory!!
    }
}