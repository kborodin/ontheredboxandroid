package ontheredbox.com.android.utils

import androidx.annotation.NonNull
import com.google.gson.JsonObject

object CreateJsonParamUtils {
    private const val EMAIL = "email"
    private const val PASSWORD = "password"
    private const val FIRST_NAME = "first_name"
    private const val LAST_NAME = "last_name"
    private const val CHECKBOX = "checkbox"
    private const val CONFIRM_PASSWORD = "confirm_password"
    private const val TOKEN = "token"

    @NonNull
    fun createAuthorizationParam(email: String, password: String): JsonObject {
        val param = JsonObject()
        param.addProperty(EMAIL, email)
        param.addProperty(PASSWORD, password)
        return param
    }

    @NonNull
    fun createRegistrationParam(email: String, firstName: String, lastName: String, checkbox: Boolean,
                                password: String, confirmPassword: String): JsonObject {
        val param = JsonObject()
        param.addProperty(EMAIL, email)
        param.addProperty(FIRST_NAME, firstName)
        param.addProperty(LAST_NAME, lastName)
        param.addProperty(CHECKBOX, checkbox)
        param.addProperty(PASSWORD, password)
        param.addProperty(CONFIRM_PASSWORD, confirmPassword)
        return param
    }

    @NonNull
    fun createGetUserParam(token:String): JsonObject {
        val param = JsonObject()
        param.addProperty(TOKEN, token)
        return param
    }
}