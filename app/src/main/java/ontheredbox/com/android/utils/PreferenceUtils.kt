package ontheredbox.com.android.utils

import android.util.Log
import androidx.annotation.NonNull
import com.orhanobut.hawk.Hawk
import ontheredbox.com.android.data.model.Categories

object PreferenceUtils {
    private const val TOKEN_KEY = "access_token"
    private const val USER_NAME_KEY = "user_name"
    private const val USER_DATA = "user_data"
    private const val EMAIL_TOKEN = "email_token"
    private const val CATEGORIES = "categories"

    fun saveToken(@NonNull token: String) {
        Log.d("token", token)
        Hawk.put(TOKEN_KEY, token)
    }

    @NonNull
    fun getToken(): String {
        return Hawk.get(TOKEN_KEY, "")
    }

    fun deleteToken() {
        Hawk.delete(TOKEN_KEY)
    }

    fun saveUserName(@NonNull userName: String) {
        Hawk.put(USER_NAME_KEY, userName)
    }

    fun saveEmailToken(emailToken: Any) {
        Hawk.put(EMAIL_TOKEN, emailToken)
    }

    fun getEmailToken(): String {
        return Hawk.get(EMAIL_TOKEN, "")
    }

    fun saveCategories(categories: ArrayList<Categories.Category>) {
        Hawk.put(CATEGORIES, categories)
    }
    fun getCategories(): ArrayList<Categories.Category> {
        return Hawk.get(CATEGORIES, arrayListOf(Categories.Category("", 0, 0, "", "","","", "", "")))
    }

    fun clearCategories() {
        Hawk.delete(CATEGORIES)
    }
}