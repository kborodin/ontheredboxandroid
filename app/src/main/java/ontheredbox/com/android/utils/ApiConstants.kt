package ontheredbox.com.android.utils

object ApiConstants {

    const val API_BASE_URL = "https://ontheredbox.com/api/"
    const val ES_LANG = "es/"
    const val ENG_LANG = "en/"
    const val LOGIN = "login"
    const val REGISTER = "register"
    const val ACCOUNT = "account"
    const val COURSES = "courses"
    const val GET_USER = "/get-user"
    const val ACTIVE = "/active"
    const val COMPLETED = "/completed"
    const val CATEGORIES = "categories"
}