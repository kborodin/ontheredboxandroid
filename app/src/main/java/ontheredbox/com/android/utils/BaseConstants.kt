package ontheredbox.com.android.utils

const val COURSES_BUNDLE_KEY = "coursesKey"
const val COURSES_ALL = "coursesAll"
const val COURSES_ACTIVE = "coursesActive"
const val COURSES_COMPLETED = "coursesCompleted"

const val COURSE_ABOUT_US_ID = 10
const val COURSE_THE_DOCTRINES_OF_THE_CROSS = 8
const val FORMING_AN_EVANGELISTIC_TEAM = 7
const val PERSONAL_EVANGELISM = 6
const val ILLUSTRATED_MESSAGES = 5
const val THE_FOUR_COLUMNS = 4
const val HOT_TO_PREPARE_YOUR_TESTIMONY = 3
const val VIEW_TYPE_MESSAGE_SEND = 11
const val VIEW_TYPE_MESSAGE_RECEIVED = 12