package ontheredbox.com.android

import android.app.Application
import android.content.Context
import com.orhanobut.hawk.Hawk

class MyApp : Application() {

    private lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}