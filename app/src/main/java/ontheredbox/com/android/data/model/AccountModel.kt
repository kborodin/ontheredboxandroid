package ontheredbox.com.android.data.model

import com.google.gson.annotations.SerializedName

data class Account(
    @SerializedName("active_count")
    val activeCount: Int,
    @SerializedName("completed_count")
    val completedCount: Int,
    @SerializedName("courses")
    val courses: Courses,
    @SerializedName("courses_count")
    val coursesCount: Int,
    @SerializedName("ebooks")
    val ebooks: List<Ebook>,
    @SerializedName("ebooks_count")
    val ebooksCount: Int,
    @SerializedName("user")
    val user: User
) {
    data class Ebook(
        @SerializedName("author")
        val author: String,
        @SerializedName("category_id")
        val categoryId: Int,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("description")
        val description: String,
        @SerializedName("file_path")
        val filePath: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("language")
        val language: String,
        @SerializedName("slug")
        val slug: String,
        @SerializedName("thumbnail")
        val thumbnail: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("updated_at")
        val updatedAt: String,
        @SerializedName("user_id")
        val userId: Int
    )

    data class User(
        @SerializedName("about")
        val about: Any,
        @SerializedName("chat_slug")
        val chatSlug: Any,
        @SerializedName("city")
        val city: Any,
        @SerializedName("country")
        val country: Any,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("dob")
        val dob: Any,
        @SerializedName("email")
        val email: String,
        @SerializedName("email_token")
        val emailToken: Any,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("full_name")
        val fullName: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("is_active")
        val isActive: Int,
        @SerializedName("is_email_verified")
        val isEmailVerified: Int,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("picture")
        val picture: Any,
        @SerializedName("role")
        val role: String,
        @SerializedName("temporary_email")
        val temporaryEmail: String,
        @SerializedName("updated_at")
        val updatedAt: String
    )

    data class Courses(
        @SerializedName("current_page")
        val currentPage: Int,
        @SerializedName("data")
        val `data`: List<Data>,
        @SerializedName("first_page_url")
        val firstPageUrl: String,
        @SerializedName("from")
        val from: Int,
        @SerializedName("last_page")
        val lastPage: Int,
        @SerializedName("last_page_url")
        val lastPageUrl: String,
        @SerializedName("next_page_url")
        val nextPageUrl: Any,
        @SerializedName("path")
        val path: String,
        @SerializedName("per_page")
        val perPage: Int,
        @SerializedName("prev_page_url")
        val prevPageUrl: Any,
        @SerializedName("to")
        val to: Int,
        @SerializedName("total")
        val total: Int
    ) {
        data class Data(
            @SerializedName("brochure")
            val brochure: String,
            @SerializedName("category_id")
            val categoryId: Int,
            @SerializedName("certificate")
            val certificate: Int,
            @SerializedName("created_at")
            val createdAt: String,
            @SerializedName("description")
            val description: String,
            @SerializedName("duration")
            val duration: Int,
            @SerializedName("id")
            val id: Int,
            @SerializedName("image")
            val image: String,
            @SerializedName("is_featured")
            val isFeatured: Int,
            @SerializedName("is_paid")
            val isPaid: Int,
            @SerializedName("language")
            val language: String,
            @SerializedName("level")
            val level: String,
            @SerializedName("price")
            val price: Any,
            @SerializedName("slug")
            val slug: String,
            @SerializedName("thumbnail")
            val thumbnail: String,
            @SerializedName("title")
            val title: String,
            @SerializedName("updated_at")
            val updatedAt: String,
            @SerializedName("user_id")
            val userId: Int,
            @SerializedName("vimeo_album_id")
            val vimeoAlbumId: String
        )
    }
}