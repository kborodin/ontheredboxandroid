package ontheredbox.com.android.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Token(
    @SerializedName("token") @Expose val token: String
)

data class UserRegistration(@SerializedName("token") val token: String,
    @SerializedName("user") val user: User) {
    data class User(
        @SerializedName("about")
        val about: Any,
        @SerializedName("chat_slug")
        val chatSlug: Any,
        @SerializedName("city")
        val city: Any,
        @SerializedName("country")
        val country: Any,
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("dob")
        val dob: Any,
        @SerializedName("email")
        val email: String,
        @SerializedName("email_token")
        val emailToken: Any,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("full_name")
        val fullName: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("is_active")
        val isActive: Int,
        @SerializedName("is_email_verified")
        val isEmailVerified: Int,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("picture")
        val picture: Any,
        @SerializedName("role")
        val role: String,
        @SerializedName("temporary_email")
        val temporaryEmail: String,
        @SerializedName("updated_at")
        val updatedAt: String
    )
}
