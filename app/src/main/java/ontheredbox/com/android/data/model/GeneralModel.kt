package ontheredbox.com.android.data.model
import com.google.gson.annotations.SerializedName


data class Categories(
    @SerializedName("categories")
    val categories: List<Category>,
    @SerializedName("count")
    val count: Int
) {
    data class Category(
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("is_active")
        val isActive: Int,
        @SerializedName("language")
        val language: String,
        @SerializedName("picture")
        val picture: String,
        @SerializedName("slug")
        val slug: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("type")
        val type: String,
        @SerializedName("updated_at")
        val updatedAt: String
    )
}

data class Message(val userId: Int, val message: String)