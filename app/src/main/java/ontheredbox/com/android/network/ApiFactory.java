package ontheredbox.com.android.network;

import androidx.annotation.NonNull;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import ontheredbox.com.android.service.ApiService;
import ontheredbox.com.android.utils.ApiConstants;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {

    private static OkHttpClient sClient;

    private static ApiService sService;

    private static ApiService createAuthService() {
        return new Retrofit.Builder()
                .baseUrl(ApiConstants.API_BASE_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService.class);
    }

    private static ApiService createService() {
        return new Retrofit.Builder()
                .baseUrl(ApiConstants.API_BASE_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService.class);
    }


    public static ApiService getAuthRetrofitService() {
        //I know that double checked locking is not a good pattern, but it's enough here
        ApiService service = sService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sService;
                if (service == null) {
                    service = sService = createAuthService();
                }
            }
        }
        return service;
    }

    public static ApiService getRetrofitService() {
        //I know that double checked locking is not a good pattern, but it's enough here
        ApiService service = sService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sService;
                if (service == null) {
                    service = sService = createService();
                }
            }
        }
        return service;
    }

    public static void recreate() {
        sClient = null;
        sService = null;
        sClient = getClient();
        sService = getRetrofitService();
    }


    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .readTimeout(40, TimeUnit.SECONDS)
                .connectTimeout(40, TimeUnit.SECONDS)
                .addInterceptor(LoggingInterceptor.create())
                .addInterceptor(ApiKeyInterceptor.create())
                .build();
    }
}
