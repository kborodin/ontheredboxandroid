package ontheredbox.com.android.network;

import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ontheredbox.com.android.utils.PreferenceUtils;
import org.jetbrains.annotations.NotNull;

public final class ApiKeyInterceptor implements Interceptor {

    private static final String LOG_TAG = "ApiKeyInterceptor";

    private final String mToken;

    private ApiKeyInterceptor() {
        mToken = PreferenceUtils.INSTANCE.getToken();
    }

    @NonNull
    static Interceptor create() {
        return new ApiKeyInterceptor();
    }

    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        if (TextUtils.isEmpty(mToken)) {
            Log.d(LOG_TAG, "token is empty");
            return chain.proceed(chain.request());
        }
        Request request = chain.request().newBuilder()
                .build();

        return chain.proceed(request);
    }
}
