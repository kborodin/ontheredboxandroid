package ontheredbox.com.android.repository.courses

object CoursesRepositoryProvider {

    private lateinit var coursesRepository: CoursesRepository

    fun provideCoursesRepository(): CoursesRepository {
        if (!this::coursesRepository.isInitialized) {
            coursesRepository = CoursesRepositoryInit()
        }
        return coursesRepository
    }
}