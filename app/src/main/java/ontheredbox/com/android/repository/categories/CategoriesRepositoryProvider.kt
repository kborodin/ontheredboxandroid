package ontheredbox.com.android.repository.categories

object CategoriesRepositoryProvider {

    private lateinit var categoriesRepository: CategoriesRepository

    fun provideCategoriesRepository(): CategoriesRepository {
        if(!this::categoriesRepository.isInitialized) {
            categoriesRepository = CategoriesRepositoryInit()
        }
        return categoriesRepository
    }
}