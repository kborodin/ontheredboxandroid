package ontheredbox.com.android.repository.registration

import io.reactivex.Observable
import ontheredbox.com.android.data.model.UserRegistration
import ontheredbox.com.android.network.ApiFactory
import ontheredbox.com.android.utils.CreateJsonParamUtils
import ontheredbox.com.android.utils.PreferenceUtils
import ontheredbox.com.android.utils.RxUtils

class RegistrationRepositoryInit : RegistrationRepository {
    override fun tryToRegister(
        email: String,
        firstName: String,
        lastName: String,
        checkbox: Boolean,
        password: String,
        confirmPassword: String
    ): Observable<UserRegistration> {
        return ApiFactory.getRetrofitService()
            .tryToRegister(
                CreateJsonParamUtils.createRegistrationParam(
                    email, firstName, lastName, checkbox,
                    password, confirmPassword
                )
            )
            .flatMap { userRegistration: UserRegistration ->
                PreferenceUtils.saveEmailToken(userRegistration.user.emailToken)
                ApiFactory.recreate()
                Observable.just(userRegistration)
            }
            .compose(RxUtils.async())
    }
}