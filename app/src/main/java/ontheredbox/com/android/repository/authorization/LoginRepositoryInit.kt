package ontheredbox.com.android.repository.authorization

import io.reactivex.Observable
import ontheredbox.com.android.data.model.Token
import ontheredbox.com.android.network.ApiFactory
import ontheredbox.com.android.utils.CreateJsonParamUtils
import ontheredbox.com.android.utils.PreferenceUtils
import ontheredbox.com.android.utils.RxUtils

class LoginRepositoryInit : LoginRepository {

    override fun tryToLogin(email: String, password: String): Observable<Token> {
        return ApiFactory.getAuthRetrofitService()
            .tryToLogin(CreateJsonParamUtils.createAuthorizationParam(email, password))
            .flatMap { token: Token ->
                PreferenceUtils.saveToken(token.token)
                PreferenceUtils.saveUserName(email)
                ApiFactory.recreate()
                Observable.just(token)
            }
            .compose(RxUtils.async())
    }
}