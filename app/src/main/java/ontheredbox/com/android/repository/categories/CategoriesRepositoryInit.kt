package ontheredbox.com.android.repository.categories

import io.reactivex.Observable
import ontheredbox.com.android.data.model.Categories
import ontheredbox.com.android.network.ApiFactory
import ontheredbox.com.android.utils.RxUtils

class CategoriesRepositoryInit: CategoriesRepository {

    override fun getCategories(token: String): Observable<Categories> {
        return ApiFactory.getRetrofitService()
            .getCategories(token)
            .flatMap {
                ApiFactory.recreate()
                Observable.just(it)
            }
            .compose(RxUtils.async())
    }
}