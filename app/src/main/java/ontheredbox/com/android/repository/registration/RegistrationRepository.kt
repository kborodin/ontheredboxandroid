package ontheredbox.com.android.repository.registration

import io.reactivex.Observable
import ontheredbox.com.android.data.model.UserRegistration

interface RegistrationRepository {

    fun tryToRegister(
        email: String, firstName: String, lastName: String, checkbox: Boolean, password: String,
        confirmPassword: String
    ): Observable<UserRegistration>
}