package ontheredbox.com.android.repository.authorization

import androidx.annotation.MainThread

object LoginRepositoryProvider {
    private lateinit var loginRepository: LoginRepository

    fun provideLoginRepository():LoginRepository {
        if(!this::loginRepository.isInitialized) {
            loginRepository = LoginRepositoryInit()
        }
        return loginRepository
    }
    @MainThread
    fun init(){
        loginRepository = LoginRepositoryInit()
    }
}