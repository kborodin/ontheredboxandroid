package ontheredbox.com.android.repository.registration

import androidx.annotation.MainThread

object RegistrationRepositoryProvider {
    private lateinit var registrationRepository: RegistrationRepository

    fun provideRegistrationRepository():RegistrationRepository {
        if(!this::registrationRepository.isInitialized) {
            registrationRepository = RegistrationRepositoryInit()
        }
        return registrationRepository
    }
    @MainThread
    fun init(){
        registrationRepository = RegistrationRepositoryInit()
    }
}
