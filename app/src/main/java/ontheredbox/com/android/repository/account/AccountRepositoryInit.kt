package ontheredbox.com.android.repository.account

import io.reactivex.Observable
import ontheredbox.com.android.data.model.Account
import ontheredbox.com.android.network.ApiFactory
import ontheredbox.com.android.utils.RxUtils

class AccountRepositoryInit : AccountRepository {
    override fun getAccount(token: String): Observable<Account> {
        return ApiFactory.getRetrofitService()
            .getAccount(token)
            .flatMap {
                ApiFactory.recreate()
                Observable.just(it)
            }
            .compose(RxUtils.async())
    }

    override fun getUser(): Observable<Account.User> {
        return ApiFactory.getRetrofitService()
            .getUser()
            .flatMap {
                ApiFactory.recreate()
                Observable.just(it)
            }
            .compose(RxUtils.async())
    }
}