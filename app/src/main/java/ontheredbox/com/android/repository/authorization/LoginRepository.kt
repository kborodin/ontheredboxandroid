package ontheredbox.com.android.repository.authorization

import io.reactivex.Observable
import ontheredbox.com.android.data.model.Token

interface LoginRepository {
    fun tryToLogin(email: String, password: String): Observable<Token>
}