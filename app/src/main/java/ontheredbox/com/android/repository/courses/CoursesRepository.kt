package ontheredbox.com.android.repository.courses

import io.reactivex.Observable
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.CompletedCourses
import ontheredbox.com.android.data.model.Courses

interface CoursesRepository {
    fun getAllCourses(token: String): Observable<Courses>
    fun getActiveCourses(token: String): Observable<ActiveCourses>
    fun getCompletedCourses(token: String): Observable<CompletedCourses>
}