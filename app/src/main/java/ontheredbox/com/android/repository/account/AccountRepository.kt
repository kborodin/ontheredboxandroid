package ontheredbox.com.android.repository.account

import io.reactivex.Observable
import ontheredbox.com.android.data.model.Account

interface AccountRepository {
    fun getAccount(token: String):Observable<Account>

    fun getUser():Observable<Account.User>
}