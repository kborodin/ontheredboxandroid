package ontheredbox.com.android.repository.courses

import io.reactivex.Observable
import ontheredbox.com.android.data.model.ActiveCourses
import ontheredbox.com.android.data.model.CompletedCourses
import ontheredbox.com.android.data.model.Courses
import ontheredbox.com.android.network.ApiFactory
import ontheredbox.com.android.utils.RxUtils

class CoursesRepositoryInit : CoursesRepository {
    override fun getAllCourses(token: String): Observable<Courses> {
        return ApiFactory.getRetrofitService()
            .getAllCourses(token)
            .flatMap {
                ApiFactory.recreate()
                Observable.just(it)
            }
            .compose(RxUtils.async())
    }

    override fun getActiveCourses(token: String): Observable<ActiveCourses> {
        return ApiFactory.getRetrofitService()
            .getActiveCourses(token)
            .flatMap {
                ApiFactory.recreate()
                Observable.just(it)
            }
            .compose(RxUtils.async())
    }

    override fun getCompletedCourses(token: String): Observable<CompletedCourses> {
        return ApiFactory.getRetrofitService()
            .getCompletedCourses(token)
            .flatMap {
                ApiFactory.recreate()
                Observable.just(it)
            }
            .compose(RxUtils.async())    }
}
