package ontheredbox.com.android.repository.categories

import io.reactivex.Observable
import ontheredbox.com.android.data.model.Categories

interface CategoriesRepository {
    fun getCategories(token: String): Observable<Categories>
}