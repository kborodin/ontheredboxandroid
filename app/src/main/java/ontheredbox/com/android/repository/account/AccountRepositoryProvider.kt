package ontheredbox.com.android.repository.account

object AccountRepositoryProvider {

    private lateinit var accountRepository: AccountRepository

    fun provideAccountRepository(): AccountRepository {
        if(!this::accountRepository.isInitialized) {
            accountRepository = AccountRepositoryInit()
        }
        return accountRepository
    }
}